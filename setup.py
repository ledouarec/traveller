#!/usr/bin/python3

from setuptools import setup, find_packages


def readme():
    with open('README.rst') as f:
        return f.read()


setup(
    name='traveller',
    version='1.0.0',
    description='Algorithm to solve traveller path problem.',
    url='https://gitlab.com/ledouarec/traveller',
    license='MIT',
    classifiers=[
        'Topic :: Scientific/Engineering',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3 :: Only',
        'Operating System :: OS Independent'
    ],
    python_requires='>=3',
    packages=find_packages(),
    install_requires=[
    ],
    entry_points={
        'console_scripts': [
            'traveller=traveller.__main__:main'
        ],
    },
    include_package_data=True,
    zip_safe=False,
)
