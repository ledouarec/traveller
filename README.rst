*********
Traveller
*********

Overview
--------

This project is used to study the algorithm of traveller path in Python.

Usage
-----

To use (with caution), simply do::

    >>> python setup.py install
    >>> ./traveller

