#! python3

"""!
@brief Compute shortest traveller path.
"""

import logging


class Traveller(object):
    """!
    @brief Documentation for Traveller class.
    Traveller class is used to compute shortest traveller path.
    """

    """!
    @brief Create logger for Traveller class.
    """
    __logger = logging.getLogger()

