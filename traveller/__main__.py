#! python3

"""!
@brief Generate traveller path for given entry
"""

import argparse
import logging
import sys
from traveller import Traveller


def main():
    """!
    @brief Entry point of the traveller script
    """
    # Create logger
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    logger_handler = logging.StreamHandler(sys.stdout)
    logger_handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
    logger_handler.setLevel(logging.INFO)

    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('-v', '--verbose',
                            action='store_true',
                            help="Verbose mode",
                            dest='verbose')
        args = parser.parse_args()
        if args.verbose:
            logger_handler.setLevel(logging.DEBUG)
        logger.addHandler(logger_handler)

        traveller = Traveller()
    except Exception as err:
        logger.error(str(err))
        sys.exit(1)


if __name__ == '__main__':
    """!
    @brief Entry point of the traveller script
    Execute only if run as a script
    """
    main()
